import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    areas: [
      { id:0,title:'Dirección Academica', component:'Area1'},
      { id:1,title:'Dirección de Investigación, Desarrollo e Innovación', component:'Area2'},
      { id:2,title:'Dirección de Cultura y Desarrollo Humano', component:'Area3'},
      { id:3,title:'Direccion de Gestión del Talento Humano', component:'Area4'},
      { id:4,title:'GERENCIA DE LOGISTICA', component:'Area5'},
      { id:5,title:'GERENCIA DE MARKETING', component:'Area6'},
      { id:6,title:'GERENCIA DE TECNOLOGÍA DE LA INFORMACIÓN', component:'Area7'},
      { id:7,title:'GERENCIA DE ECONOMÍA', component:'Area8'},
    ],
  },
  mutations: {

  },
  actions: {


  }
})
